import { useState } from "react";
import {
  Button,
  Container,
  Nav,
  NavItem,
  NavLink,
  Navbar,
  NavbarBrand,
} from "reactstrap";

export default function Index() {
  const [count, setCount] = useState(0);
  return (
    <>
      <Navbar color="light" light>
        <Container>
          <NavbarBrand tag="span">
            next-aws-ecr-ecs-gitlab-ci-cd-example (version 5)
          </NavbarBrand>
          <Nav navbar>
            <NavItem>
              <NavLink href="https://gitlab.com/saltycrane/next-aws-ecr-ecs-gitlab-ci-cd-example">
                GitLab
              </NavLink>
            </NavItem>
          </Nav>
        </Container>
      </Navbar>
      <Container>
        <div className="d-flex align-items-baseline py-5">
          Demonstrate client-side JS is working: {count}
          <Button
            className="ml-3"
            color="primary"
            onClick={() => setCount((count) => count + 1)}
            size="sm"
          >
            Increment
          </Button>
        </div>
      </Container>
    </>
  );
}
